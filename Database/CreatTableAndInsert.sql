create database Commodity
on primay
(
	name = 'Commodity',
	size = 20MB,
	filegrowth = 5M,
	maxsize = 500MB,
	filename = 'c:\Program Files\...\Commodity.mdf'
)

log on
	name = 'Commodity_log',
	size = 5MB,
(
	filegrowth = 10%,
	maxsize = 25MB,
	filename = 'c:\Program Files\...\Commodity.ldf'
);


use Commodity;

drop table if exists Commodity;
create table Commodity
(
	-- column_name data_type constraint_type(s)
	CommodityID int  IDENTITY(1,1) not null, -- Auto increment generated number
	CommodityName nvarchar(50) not null,
	CommodityType nvarchar(10) not null,
	BuyingPrice decimal(10,2) null,
	SalePrice decimal(10,2) null,
	IsDiscount bit,
	InStock int not null,
	Supplier int null
	-- constraint constraint_name constraint_type
	constraint pk_CommodityID primary key clustered 
	(
		CommodityID asc
	)	
);

drop table if exists Supplier;
create table Supplier
(
	SupplierID int IDENTITY(1,1) not null,
	SupplierName nvarchar(50) not null,
	SupplierPhone nvarchar(15) not null
	constraint pk_SupplierID primary key clustered 
	(
		SupplierID asc
	)	
);

alter table Commodity
	add constraint fk_Commodity_Supplier foreign key (Supplier)
		references Supplier (SupplierID)
;

drop table if exists UserInfo;
create table UserInfo 
(
	UserId int not null,
	UserName nvarchar(50) not null,
	Password nvarchar(50) not null
)

insert into UserInfo(UserId, UserName, Password) values(1, 'admin', '123');
insert into UserInfo(UserId, UserName, Password) values(2, 'user', '123');


Select * from Supplier;
insert into Supplier(SupplierName, SupplierPhone) values('Imperial Tobacco Canada Limited', '+1 800-932-9326');
insert into Supplier(SupplierName, SupplierPhone) values('Molson Coors Brewing Company', '416-679-7004');
insert into Supplier(SupplierName, SupplierPhone) values('LotoQuebec', '1-866-611-5686');
insert into Supplier(SupplierName, SupplierPhone) values('Lays Canada', '1-800-376-2257');
insert into Supplier(SupplierName, SupplierPhone) values('Doritos Canada', '1-800-376-2257');
insert into Supplier(SupplierName, SupplierPhone) values('AGROPUR Dairy Cooperative', '1 800 361-1110');

insert into Commodity( CommodityName, CommodityType, BuyingPrice, SalePrice, IsDiscount, InStock, Supplier) values('Avanti du Maurier Light Slim King Size 20', 'Cigarette', 10, 12.5, 'false', 3,1);
insert into Commodity(CommodityName, CommodityType, BuyingPrice, SalePrice, IsDiscount, InStock, Supplier) values('Avanti du Maurier Light Slim King Size 25', 'Cigarette', 13, 15, 'false', 5,1);
insert into Commodity(CommodityName, CommodityType, BuyingPrice, SalePrice, IsDiscount, InStock, Supplier) values('Avanti du Maurier Light Slim 100MM 25', 'Cigarette', 10, 12.5, 'false', 2,1);
insert into Commodity(CommodityName, CommodityType, BuyingPrice, SalePrice, IsDiscount, InStock, Supplier) values('Belmont King Size 20', 'Cigarette', 12, 13.65, 'false', 2,1);
insert into Commodity(CommodityName, CommodityType, BuyingPrice, SalePrice, IsDiscount, InStock, Supplier) values('Belmont Regular 20', 'Cigarette', 10, 13.65, 'false', 2,1);
insert into Commodity(CommodityName, CommodityType, BuyingPrice, SalePrice, IsDiscount, InStock, Supplier) values('Belmont Silver LK25', 'Cigarette', 13, 15.99, 'false', 1,1);
insert into Commodity(CommodityName, CommodityType, BuyingPrice, SalePrice, IsDiscount, InStock, Supplier) values('Belmont Silver Reg 20','Cigarette', 12, 13.65, 'false', 2,1);
insert into Commodity( CommodityName, CommodityType, BuyingPrice, SalePrice, IsDiscount, InStock, Supplier) values('Belmont Silver King Size 20', 'Cigarette', 13, 15.65, 'false', 5,1);
insert into Commodity(CommodityName, CommodityType, BuyingPrice, SalePrice, IsDiscount, InStock, Supplier) values('Belvedere Regular', 'Cigarette', 13, 15, 'false', 3,1);
insert into Commodity( CommodityName, CommodityType, BuyingPrice, SalePrice, IsDiscount, InStock, Supplier) values('Heineken Cans 6*330 ml', 'Beer', 13, 14.95, 'false', 0,2);
insert into Commodity(CommodityName, CommodityType, BuyingPrice, SalePrice, IsDiscount, InStock, Supplier) values('Heineken Cans 12*330 ml', 'Beer', 26, 27.95, 'false', 3,2);
insert into Commodity(CommodityName, CommodityType, BuyingPrice, SalePrice, IsDiscount, InStock, Supplier) values('Heineken Cans 24*330 ml', 'Beer', 39, 49.95, 'false', 6,2);
insert into Commodity(CommodityName, CommodityType, BuyingPrice, SalePrice, IsDiscount, InStock, Supplier) values('Heineken Bottles 6*330 ml', 'Beer', 13, 14.95, 'false', 0,2);
insert into Commodity(CommodityName, CommodityType, BuyingPrice, SalePrice, IsDiscount, InStock, Supplier) values('Heineken Bottles 12*330 ml', 'Beer', 26, 27.95, 'false', 3,2);
insert into Commodity(CommodityName, CommodityType, BuyingPrice, SalePrice, IsDiscount, InStock, Supplier) values('Heineken Bottles 24*330 ml', 'Beer', 39, 49.95, 'false', 6,2);
insert into Commodity(CommodityName, CommodityType, BuyingPrice, SalePrice, IsDiscount, InStock, Supplier) values('Heineken Bottles 12*650 ml', 'Beer', 26, 43.95, 'false', 3,2);
insert into Commodity(CommodityName, CommodityType, BuyingPrice, SalePrice, IsDiscount, InStock, Supplier) values('Heineken Bottles 1*1500 ml', 'Beer', 13, 15, 'true', 6,2);
insert into Commodity(CommodityName, CommodityType, BuyingPrice, SalePrice, IsDiscount, InStock, Supplier) values('Riches', 'Lotteries', 25, 30, 'false', 10,3);
insert into Commodity(CommodityName, CommodityType, BuyingPrice, SalePrice, IsDiscount, InStock, Supplier) values('777', 'Lotteries', 4, 5, 'false', 30,3);
insert into Commodity(CommodityName, CommodityType, BuyingPrice, SalePrice, IsDiscount, InStock, Supplier) values('Bingo', 'Lotteries', 2.5, 3, 'false', 20,3);
insert into Commodity(CommodityName, CommodityType, BuyingPrice, SalePrice, IsDiscount, InStock, Supplier) values('CocaCola cans', 'Groceries', 0.5, 1, 'false', 20,null);
