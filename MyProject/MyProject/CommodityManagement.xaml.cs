﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MyProject
{
    /// <summary>
    /// Interaction logic for CommodityManagement.xaml
    /// </summary>
    public partial class CommodityManagement : Window
    {
        public readonly CommodityInfo info = new CommodityInfo();
        public static ModelView modelView = new ModelView();
        CommodityEntities context = new CommodityEntities();

        public CommodityManagement()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            if (txtCommodityName.Text.Trim() == "" || cbType.SelectedItem == null || txtBuyingPrice.Text.Trim() == "" || txtSalePrice.Text.Trim() == "" || txtInStock.Text.Trim() == "")
            {
                MessageBox.Show("Please fill all fields");
                return;
            }

            if (Decimal.Parse(txtBuyingPrice.Text) > Decimal.Parse(txtSalePrice.Text))
            {
                MessageBox.Show("Sale price should greater than buy price");
                return;
            }

            try
            {
                string discount = string.Empty;
                if (rbNo.IsChecked == true)
                {
                    discount = "false";
                }
                else
                {
                    discount = "true";
                }

                if (txtSuppllier.Text.Trim() == "")
                {
                    Commodity comm = new Commodity()
                    {
                        //CommodityID = Int32.Parse(txtCommodityID.Text.Trim()),
                        CommodityName = txtCommodityName.Text.Trim(),
                        CommodityType = cbType.SelectionBoxItem.ToString(),
                        SalePrice = Decimal.Parse(txtBuyingPrice.Text.Trim()),
                        BuyingPrice = Decimal.Parse(txtSalePrice.Text.Trim()),
                        IsDiscount = Boolean.Parse(discount),
                        InStock = Int32.Parse(txtInStock.Text.Trim()),
                    };
                    context.Commodities.Add(comm);
                    context.SaveChanges();
                    loadgrid();
                    MessageBox.Show("Add Sucess");
                }
                else
                {
                    Commodity comm = new Commodity()
                    {
                        //CommodityID = Int32.Parse(txtCommodityID.Text.Trim()),
                        CommodityName = txtCommodityName.Text.Trim(),
                        CommodityType = cbType.SelectionBoxItem.ToString(),
                        SalePrice = Decimal.Parse(txtBuyingPrice.Text.Trim()),
                        BuyingPrice = Decimal.Parse(txtBuyingPrice.Text.Trim()),
                        IsDiscount = Boolean.Parse(discount),
                        InStock = Int32.Parse(txtInStock.Text.Trim()),
                        Supplier = Int32.Parse(txtSuppllier.Text.Trim())
                    };
                    context.Commodities.Add(comm);
                    context.SaveChanges();
                    loadgrid();
                    MessageBox.Show("Add Sucess");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            Clear();
        }


        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            string discount = string.Empty;
            if (rbNo.IsChecked == true)
            {
                discount = "false";
            }
            else
            {
                discount = "true";
            }

            try
            {
                if (txtSuppllier.Text.Trim() == "")
                {
                    Commodity comm = new Commodity()
                    {
                        CommodityID = Int32.Parse(txtCommodityID.Text.Trim()),
                        CommodityName = txtCommodityName.Text.Trim(),
                        CommodityType = cbType.SelectionBoxItem.ToString(),
                        SalePrice = Decimal.Parse(txtBuyingPrice.Text.Trim()),
                        BuyingPrice = Decimal.Parse(txtBuyingPrice.Text.Trim()),
                        IsDiscount = Boolean.Parse(discount),
                        InStock = Int32.Parse(txtInStock.Text.Trim()),
                    };
                    bool result = info.UpdateCommodity(comm);
                    if (result)
                    {
                        CommodityEntities con = new CommodityEntities();
                        var data = from c in con.Commodities select c;
                        dgDisplay.ItemsSource = data.ToList();
                        MessageBox.Show("Update Sucess");

                    }
                }
                else
                {
                    Commodity comm = new Commodity()
                    {
                        CommodityID = Int32.Parse(txtCommodityID.Text.Trim()),
                        CommodityName = txtCommodityName.Text.Trim(),
                        CommodityType = cbType.SelectionBoxItem.ToString(),
                        SalePrice = Decimal.Parse(txtBuyingPrice.Text.Trim()),
                        BuyingPrice = Decimal.Parse(txtBuyingPrice.Text.Trim()),
                        IsDiscount = Boolean.Parse(discount),
                        InStock = Int32.Parse(txtInStock.Text.Trim()),
                        Supplier = Int32.Parse(txtSuppllier.Text.Trim())
                    };
                    bool result = info.UpdateCommodity(comm);
                    if (result)
                    {
                        CommodityEntities con = new CommodityEntities();
                        var data = from c in con.Commodities select c;
                        dgDisplay.ItemsSource = data.ToList();
                        MessageBox.Show("Update Sucess");
                    }
                }
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
            Clear();
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult messageBoxResult = MessageBox.Show($"Deleting user ID: {txtCommodityID.Text}\nAre you sure?",
                                                               "Confirm Delete",
                                                               MessageBoxButton.YesNo,
                                                               MessageBoxImage.Warning);
            
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                bool result = info.DeleteCommodity(Int32.Parse(txtCommodityID.Text));
                loadgrid();
                MessageBox.Show("Deleted Sucess!");
            }
            
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            Clear();
        }

        private void Clear()
        {
            txtCommodityID.Text = "";
            txtCommodityName.Text = "";
            cbType.SelectedItem = null;
            txtBuyingPrice.Text = "";
            txtSalePrice.Text = "";

            rbYes.IsChecked = false;
            rbNo.IsChecked = false;

            txtInStock.Text = "";
            txtSuppllier.Text = "";

            btnAdd.IsEnabled = true;
            btnDelete.IsEnabled = false;
            btnEdit.IsEnabled = false;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            loadgrid();
        }

        public void loadgrid()
        {
            var data = from c in context.Commodities select c;
            dgDisplay.ItemsSource = data.ToList();
        }

        private void dgDisplay_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            if (dgDisplay.SelectedIndex != -1)
            {
                txtCommodityID.Text = ((Commodity)dgDisplay.SelectedItem).CommodityID.ToString();
                txtCommodityName.Text = ((Commodity)dgDisplay.SelectedItem).CommodityName.ToString();
                txtBuyingPrice.Text = ((Commodity)dgDisplay.SelectedItem).BuyingPrice.ToString();
                txtSalePrice.Text = ((Commodity)dgDisplay.SelectedItem).SalePrice.ToString();
                txtInStock.Text = ((Commodity)dgDisplay.SelectedItem).InStock.ToString();
                txtSuppllier.Text = ((Commodity)dgDisplay.SelectedItem).Supplier.ToString();
                cbType.Text = ((Commodity)dgDisplay.SelectedItem).CommodityType.ToString();
                rbYes.IsChecked = Boolean.Parse(((Commodity)dgDisplay.SelectedItem).IsDiscount.ToString());
                if (rbYes.IsChecked == true)
                {
                    rbNo.IsChecked = false;
                }
                else
                {
                    rbNo.IsChecked = true;
                }
            }
            btnAdd.IsEnabled = true;
            btnDelete.IsEnabled = true;
            btnEdit.IsEnabled = true;
        }

        private void miCigarette_Click(object sender, RoutedEventArgs e)
        {
            var data = from c in context.Commodities where c.CommodityType == "Cigarette" select c;
            dgDisplay.ItemsSource = data.ToList();
        }

        private void miBeer_Click(object sender, RoutedEventArgs e)
        {
            var data = from c in context.Commodities where c.CommodityType == "Beer" select c;
            dgDisplay.ItemsSource = data.ToList();
        }

        private void miLottery_Click(object sender, RoutedEventArgs e)
        {
            var data = from c in context.Commodities where c.CommodityType == "Lotteries" select c;
            dgDisplay.ItemsSource = data.ToList();
        }

        private void miAll_Click(object sender, RoutedEventArgs e)
        {
            loadgrid();
        }

        private void miGrocery_Click(object sender, RoutedEventArgs e)
        {
            var data = from c in context.Commodities where c.CommodityType == "Groceries" select c;
            dgDisplay.ItemsSource = data.ToList();
        }

        private void miSupplier_Click(object sender, RoutedEventArgs e)
        {
            SupplierInfo supplier = new SupplierInfo();
            supplier.Show();
            this.Close();
        }

        private void btnFind_Click(object sender, RoutedEventArgs e)
        {

            string pattern = @"^\d+(\.\d)?$";
            
            if (!Regex.IsMatch(txtFind.Text.Trim(), pattern))
            {
                string searchName = txtFind.Text.Trim();
                var data = from c in context.Commodities
                           where c.CommodityName.Contains(searchName)
                           select c;
                dgDisplay.ItemsSource = data.ToList();
                Clear();
                
            }
            else
            {
                int searchID = Int32.Parse(txtFind.Text.Trim());
                var data = from c in context.Commodities
                           where (c.CommodityID == searchID)
                           select c;
                dgDisplay.ItemsSource = data.ToList();
                Clear();
            }          
        }
    }
}