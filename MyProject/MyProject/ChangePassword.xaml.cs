﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MyProject
{
    /// <summary>
    /// Interaction logic for ChangePassword.xaml
    /// </summary>
    public partial class ChangePassword : Window
    {
        public ChangePassword()
        {
            InitializeComponent();
        }

        public string UserName { get; set; }
        public string Password { get; set; }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            string currentPassword = txtCurrentPassword.Password.Trim();
            string newPassword = txtNewPassword.Password.Trim();
            string verifyPassword = txtVerifyPassword.Password.Trim();

            if (currentPassword == "" || newPassword == "" || verifyPassword == "")
            {
                MessageBox.Show("Please enter your username and password");
            } 
            else
            {
                if (currentPassword != Password)
                {
                    MessageBox.Show("Password is not correct");
                    return;
                }
                if (newPassword != verifyPassword)
                {
                    MessageBox.Show("Please keep these two password the same");
                    return;
                }

                using (CommodityEntities context = new CommodityEntities())
                {
                    var result = context.UserInfoes.SingleOrDefault
                             (u => u.UserName == UserName);
                    if (result != null)
                    {
                        result.Password = txtNewPassword.Password;
                        context.SaveChanges();
                        MessageBox.Show("Change Password Sucess And Please Login Again");
                        Login loginScreen = new Login();
                        loginScreen.Show();
                        this.Close();
                    }
                }
            }              
        }
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {           
            User user = new User();
            user.UserName = UserName;
            user.Password = Password;
            user.Show();
            this.Close();
        }
    }
}
