﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MyProject
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        
        public Login()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txtUsername.Text))
            {
                MessageBox.Show("Please Enter User Name");
                txtUsername.Focus();
                return;
            }
            try
            {
                using (CommodityEntities context = new CommodityEntities())
                {
                    var query = from u in context.UserInfoes
                                where u.UserName == txtUsername.Text && u.Password == txtPassword.Password
                                select u;
                    if(query != null)
                    {
                        User user = new User();
                        user.UserName = txtUsername.Text;
                        user.Password = txtPassword.Password;
                        user.Show();
                        this.Close();
                    }
                }
            }
            catch(Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            txtPassword.Password = String.Empty;
            txtUsername.Text = String.Empty;
        }
    }
}
