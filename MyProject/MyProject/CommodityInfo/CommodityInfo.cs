﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MyProject
{
    public class CommodityInfo
    {
        CommodityEntities context = new CommodityEntities();

        public List<Commodity> GetCommodity()
        {
            var CommQuery = from commodity in context.Commodities
                            select commodity;
            return CommQuery.ToList<Commodity>();
        }

        public Commodity GetCommodityById(int commID)
        {
            return context.Commodities.Find(commID);
        }

        public int AddCommodity(Commodity comm)
        {
            try
            {
                context.Commodities.Add(comm);
                context.SaveChanges();
                return comm.CommodityID;
            }
            catch
            {
                return -1;
            }
        }

        public bool DeleteCommodity(int commId)
        {
            try
            {
                Commodity commodity = GetCommodityById(commId);
                context.Commodities.Remove(commodity);
                context.SaveChanges();
                return true;
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
                return false;
            }
        }

        public bool UpdateCommodity(Commodity comm)
        {
            try
            {
                Commodity oldCommodity = GetCommodityById(comm.CommodityID);
                oldCommodity.CommodityName = comm.CommodityName;
                oldCommodity.CommodityType = comm.CommodityType;
                oldCommodity.BuyingPrice = comm.BuyingPrice;
                oldCommodity.SalePrice = comm.SalePrice;
                oldCommodity.IsDiscount = comm.IsDiscount;
                oldCommodity.Supplier = comm.Supplier;
                oldCommodity.InStock = comm.InStock;
                context.SaveChanges();
                return true;
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
                return false;
            }
        }

        public List<Commodity> FindCommodity(Commodity comm)
        {
            var commodity = context.Commodities.Where(c => true);
            if (comm.CommodityID != 0)
            {
                commodity = commodity.Where(c => c.CommodityID == comm.CommodityID);
            }

            if (comm.CommodityName != null)
            {
                commodity = commodity.Where(c => c.CommodityName.Contains(comm.CommodityName));
            }
            if (comm.CommodityType != null)
            {
                commodity = commodity.Where(c => c.CommodityType.Contains(comm.CommodityType));
            }
            if (comm.InStock != 0)
            {
                commodity = commodity.Where(c => c.InStock == comm.InStock);
            }
            if (comm.BuyingPrice != 0)
            {
                commodity = commodity.Where(c => c.BuyingPrice == comm.BuyingPrice);
            }
            if (comm.SalePrice != 0)
            {
                commodity = commodity.Where(c => c.SalePrice == comm.SalePrice);
            }
            if (comm.IsDiscount != null)
            {
                commodity = commodity.Where(c => c.IsDiscount.Equals(comm.IsDiscount));
            }
            if (comm.Supplier != 0)
            {
                commodity = commodity.Where(c => c.Supplier == comm.Supplier);
            }
            return commodity.ToList();
        }
    }
}

