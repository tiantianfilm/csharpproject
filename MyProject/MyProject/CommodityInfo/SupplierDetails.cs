﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MyProject
{
    public class SupplierDetails
    {
        CommodityEntities context = new CommodityEntities();

        public List<Supplier> GetSupplier()
        {
            var SupQuery = from supplier in context.Suppliers
                           select supplier;
            return SupQuery.ToList<Supplier>();
        }

        public Supplier GetSupplierById(int supID)
        {
            return context.Suppliers.Find(supID);
        }

        public bool DeleteSupplier(int supId)
        {
            try
            {
                Supplier supplier = GetSupplierById(supId);
                context.Suppliers.Remove(supplier);
                context.SaveChanges();
                return true;
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
                return false;
            }
        }

        public bool UpdateSupplier(Supplier sup)
        {
            try
            {
                Supplier oldSupplier = GetSupplierById(sup.SupplierID);
                oldSupplier.SupplierName = sup.SupplierName;
                oldSupplier.SupplierPhone = sup.SupplierPhone;
                context.SaveChanges();
                return true;
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
                return false;
            }
        }

        public List<Supplier> FindSupplier(Supplier sup)
        {
            var supplier = context.Suppliers.Where(c => true);
            if (sup.SupplierID != 0)
            {
                supplier = supplier.Where(c => c.SupplierID == sup.SupplierID);
            }
            if (sup.SupplierName != null)
            {
                supplier = supplier.Where(c => c.SupplierName.Contains(sup.SupplierName));
            }
            if (sup.SupplierPhone != null)
            {
                supplier = supplier.Where(c => c.SupplierPhone.Contains(sup.SupplierPhone));
            }
            return supplier.ToList();
        }
    }
}
