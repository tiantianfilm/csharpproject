﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MyProject
{
    /// <summary>
    /// Interaction logic for User.xaml
    /// </summary>
    public partial class User : Window
    {
        public User()
        {
            InitializeComponent();
        }
        public int UserID { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        public User(string username)
        {
            this.UserName = username;
        }

        private void miChangePassword_Click(object sender, RoutedEventArgs e)
        {
            ChangePassword change = new ChangePassword();
            change.UserName = this.UserName;
            change.Password = Password;
            change.Show();
            this.Close();
        }

        private void miProductmanage_Click(object sender, RoutedEventArgs e)
        {
            CommodityManagement commodityManagement = new CommodityManagement();
            commodityManagement.Show();
            this.Close();
        }
    }
}
