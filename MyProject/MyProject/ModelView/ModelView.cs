﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyProject
{
    public class ModelView 
    {
        private List<Commodity> commodities;
        public List<Commodity> Commodities
        {
            get { return commodities; }
            set
            {
                commodities = value;
            }
        }
      
        public void UpdateCommodityInList(Commodity comm)
        {
            Commodity commodityToBeChanged = Commodities.Where(c => c.CommodityID == comm.CommodityID).FirstOrDefault();
            commodityToBeChanged.CommodityName = comm.CommodityName;
            commodityToBeChanged.CommodityType = comm.CommodityType;
            commodityToBeChanged.BuyingPrice = comm.BuyingPrice;
            commodityToBeChanged.SalePrice = comm.SalePrice;
            commodityToBeChanged.IsDiscount = comm.IsDiscount;
            commodityToBeChanged.InStock = comm.InStock;
            commodityToBeChanged.Supplier = comm.Supplier;
        }
    }
}
