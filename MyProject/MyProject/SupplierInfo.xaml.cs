﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MyProject
{
    /// <summary>
    /// Interaction logic for SupplierInfo.xaml
    /// </summary>
    public partial class SupplierInfo : Window
    {
        SupplierDetails supInfo = new SupplierDetails();
        CommodityEntities context = new CommodityEntities();

        public SupplierInfo()
        {
            InitializeComponent();
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            if (txtSupplierName.Text.Trim() == "" || txtSupplierPhone.Text.Trim() == "")
            {
                MessageBox.Show("Please enter all fields");
                return;
            }

            Supplier supp = new Supplier()
            {
                SupplierID = Int32.Parse(txtSupplierID.Text.Trim()),
                SupplierName = txtSupplierName.Text.Trim(),
                SupplierPhone = txtSupplierPhone.Text.Trim()
            };

            bool result = supInfo.UpdateSupplier(supp);
            if (result)
            {
                CommodityEntities con = new CommodityEntities();
                var data = from c in con.Suppliers select c;
                dgSupplier.ItemsSource = data.ToList();
                MessageBox.Show("Update Sucess");
            }
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult messageBoxResult = MessageBox.Show($"Deleting user ID: {txtSupplierID.Text}\nAre you sure?",
                                                               "Confirm Delete",
                                                               MessageBoxButton.YesNo,
                                                               MessageBoxImage.Warning);

            if (messageBoxResult == MessageBoxResult.Yes)
            {
                bool result = supInfo.DeleteSupplier(Int32.Parse(txtSupplierID.Text));
                load();
                MessageBox.Show("Deleted Sucess!");
                
            }
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            txtSupplierID.Text = "";
            txtSupplierName.Text = "";
            txtSupplierPhone.Text = "";
        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            CommodityManagement management = new CommodityManagement();
            management.Show();
            this.Close();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            if (txtSupplierName.Text.Trim() == "" || txtSupplierPhone.Text.Trim() == "")
            {
                MessageBox.Show("Please enter all fields");
                return;
            }
            try
            {
                Supplier supplier = new Supplier()
                {
                    //SupplierID = Int32.Parse(txtSupplierID.Text.Trim()),
                    SupplierName = txtSupplierName.Text.Trim(),
                    SupplierPhone = txtSupplierPhone.Text.Trim()
                };
                context.Suppliers.Add(supplier);
                context.SaveChanges();
                load();
                MessageBox.Show("Added Sucess!");
            }
            catch(Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
        }

        public void load()
        {
            var data = from c in context.Suppliers select c;
            dgSupplier.ItemsSource = data.ToList();            
        }

        private void dgSupplier_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgSupplier.SelectedIndex != -1)
            {
                txtSupplierID.Text = ((Supplier)dgSupplier.SelectedItem).SupplierID.ToString();
                txtSupplierName.Text = ((Supplier)dgSupplier.SelectedItem).SupplierName.ToString();
                txtSupplierPhone.Text = ((Supplier)dgSupplier.SelectedItem).SupplierPhone.ToString();               
            }
            btnAdd.IsEnabled = true;
            btnDelete.IsEnabled = true;
            btnEdit.IsEnabled = true;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            load();
        }
    }
}
